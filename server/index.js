﻿const port = 80;
const express = require("express");
const parser = require("body-parser");
const app = express();

app.use(parser.json());

app.use((request, response, next) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
	response.setHeader('Access-Control-Allow-Methods', 'POST');
	response.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.post("/", (request, response) => {
	var f = new Function("x", "y", "with(Math){return " + request.body.expression + ";}");
	var result = [];
	for(var x = request.body.x1; x <= request.body.x2; x += request.body.dx) {
		for(var y = request.body.y1; y <= request.body.y2; y += request.body.dy) {
			var z = f(x, y);
			result.push({
				"x": x,
				"y": y,
				"z": z
			});
		}
	}
	response.json(result);
});

app.listen(port, (error) => {
	if(error) {
		return console.log("ERROR", error);
	}
	console.log(`Server start successfull on port ${port}`);
});
