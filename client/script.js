var camera, scene, renderer, group;

window.onload = function () {
    init();
    animate();
    document.getElementById("calc").onclick = function () {
        var inputData = {
            expression: document.getElementById("expression").value,
            x1: parseFloat(document.getElementById("x1").value),
            x2: parseFloat(document.getElementById("x2").value),
            dx: parseFloat(document.getElementById("dx").value),
            y1: parseFloat(document.getElementById("y1").value),
            y2: parseFloat(document.getElementById("y2").value),
            dy: parseFloat(document.getElementById("dy").value)
        };
        var request = new XMLHttpRequest();
        request.open("POST", "http://10.1.2.68/", true);
        request.onreadystatechange = function() {
            if(request.readyState != 4) {
                return;
            }
            clearTimeout(timeout);
            if(request.status == 200) {
                var outputData = JSON.parse(request.responseText);
                var str = "x\ty\tz";
                for(var i = 0; i < outputData.length; i++) {
                    str += "\n" + outputData[i].x;
                    str += "\t" + outputData[i].y;
                    str += "\t" + outputData[i].z;
                }
                alert(str);
                // TODO: реализовать трёхмерную визуализацию полученных данных
            } else {
                alert("Сервер вернул код состояния " + request.status + " и тело сообщения " + request.responseText);
            }
        };
        request.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        request.send(JSON.stringify(inputData));
        var timeout = setTimeout(function() {
            request.abort();
            alert("Истекли 30 секунд ожидания ответа от сервера");
        }, 30000);
    }
}

function init() {
    camera = new THREE.PerspectiveCamera(75, 640 / 480, 1, 1000);
    camera.position.z = 600;
    scene = new THREE.Scene();
    group = new THREE.Group();
    group.rotation.x = 0.7;
    scene.add(group);
    var ambientLight = new THREE.AmbientLight(Math.random() * 0x10);
    scene.add(ambientLight);
    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.x = -50;
    directionalLight.position.y = -50;
    directionalLight.position.z = 150;
    scene.add(directionalLight);
    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.x = 200;
    directionalLight.position.y = 200;
    directionalLight.position.z = 50;
    scene.add(directionalLight);
    renderer = new THREE.WebGLRenderer({ alpha: true });
    renderer.setSize(640, 480);
    renderer.setClearColor(0x0000ff, 0.5);
    document.getElementById("view").appendChild(renderer.domElement);
}

function animate() {
    requestAnimationFrame(animate);
    group.rotation.y += 0.01;
    renderer.render(scene, camera);
}
