var camera, scene, renderer, group;

window.onload = function () {
    init();
    animate();
}

// инициализация начальных значений
function init() {
    // создаем камеру - перспективная проекция
    camera = new THREE.PerspectiveCamera(75, 640 / 480, 1, 1000);
    // установка z-координаты камеры
    camera.position.z = 600;
    // настройка сцены
    scene = new THREE.Scene();

    group = new THREE.Group();
    // настройка материала - установка цвета
    var material = new THREE.MeshLambertMaterial({color: 0xff7f00, overdraw: 0.5});
    // настройка геометрии - в качестве геометрии будет куб
    // настроим его ширину, высоту и длину по оси z
    var geometry = new THREE.CubeGeometry(100, 100, 100);
    // настраиваем меш, который будет отображать куб
    var mesh = new THREE.Mesh(geometry, material);
    group.add(mesh);
    material = new THREE.MeshLambertMaterial({color: 0x007fff, overdraw: 0.5});
    geometry = new THREE.CylinderGeometry(50, 50, 100, 3);
    mesh = new THREE.Mesh(geometry, material);
    mesh.position.x += 150;
    group.add(mesh);
    geometry = new THREE.ConeGeometry(50, 100, 4);
    mesh = new THREE.Mesh(geometry, material);
    mesh.position.x -= 150;
    group.add(mesh);
    material = new THREE.MeshLambertMaterial({color: 0xff007f, overdraw: 0.5});
    geometry = new THREE.SphereGeometry(50, 24, 12);
    mesh = new THREE.Mesh(geometry, material);
    mesh.position.z -= 150;
    group.add(mesh);
    geometry = new THREE.TorusGeometry(50, 15, 12, 24);
    mesh = new THREE.Mesh(geometry, material);
    mesh.position.z += 150;
    mesh.position.y -= 50;
    mesh.rotation.x = 1.57
    group.add(mesh);
    material = new THREE.MeshLambertMaterial({color: 0x007f00, overdraw: 0.5});
    geometry = new THREE.Geometry();
    geometry.vertices.push(
        new THREE.Vector3(50, 0, 0),
        new THREE.Vector3(0, 50, 0),
        new THREE.Vector3(-50, 0, 0),
        new THREE.Vector3(0, -50, 0)
    );
    geometry.faces.push(new THREE.Face3(0, 1, 2, new THREE.Vector3(0, 0, 1)));
    geometry.faces.push(new THREE.Face3(2, 3, 0, new THREE.Vector3(0, 0, 1)));
    mesh = new THREE.Mesh(geometry, material);
    mesh.position.y += 100;
    group.add(mesh);

    group.rotation.x = 0.5;
    scene.add(group);

    var ambientLight = new THREE.AmbientLight(Math.random() * 0x10);
    scene.add(ambientLight);
    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.x = -50;
    directionalLight.position.y = -50;
    directionalLight.position.z = 150;
    scene.add(directionalLight);
    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.x = 200;
    directionalLight.position.y = 200;
    directionalLight.position.z = 50;
    scene.add(directionalLight);

    // создаем объект для рендеринга сцены
    renderer = new THREE.WebGLRenderer({ alpha: true });
    // установка размеров
    renderer.setSize(640, 480);
    renderer.setClearColor(0x0000ff, 0.5);
    // встраиваем в DOM-структуру страницы
    document.getElementById("view").appendChild(renderer.domElement);
}

// функция анимации
function animate() {
    requestAnimationFrame(animate);
    // вращение группы вокруг осей
    group.rotation.y += 0.01;
    // рендеринг сцены - метод, производящий по сути отрисовку
    renderer.render(scene, camera);
}
